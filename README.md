# Dynamic DNS

based on [this blog post](http://mkaczanowski.com/golang-build-dynamic-dns-service-go/)

Run:

```
go run main.go -port 10053 -tsig hello:aGVsbG93b3JsZAo=
```

Then use req.txt with nsupdate. To install nsupdate and dig, `sudo apt install dnsutils`

```
nsupdate req.txt
```

Query the server

```
dig -p 10053 @localhost test.mydomain.com. A
```


